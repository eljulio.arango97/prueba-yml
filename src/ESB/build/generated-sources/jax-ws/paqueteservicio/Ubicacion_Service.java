
package paqueteservicio;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.11-b150120.1832
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ubicacion", targetNamespace = "http://paqueteServicio/", wsdlLocation = "http://localhost:54536/ServicioUbicacion/ubicacion?wsdl")
public class Ubicacion_Service
    extends Service
{

    private final static URL UBICACION_WSDL_LOCATION;
    private final static WebServiceException UBICACION_EXCEPTION;
    private final static QName UBICACION_QNAME = new QName("http://paqueteServicio/", "ubicacion");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:54536/ServicioUbicacion/ubicacion?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        UBICACION_WSDL_LOCATION = url;
        UBICACION_EXCEPTION = e;
    }

    public Ubicacion_Service() {
        super(__getWsdlLocation(), UBICACION_QNAME);
    }

    public Ubicacion_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), UBICACION_QNAME, features);
    }

    public Ubicacion_Service(URL wsdlLocation) {
        super(wsdlLocation, UBICACION_QNAME);
    }

    public Ubicacion_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, UBICACION_QNAME, features);
    }

    public Ubicacion_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public Ubicacion_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns Ubicacion
     */
    @WebEndpoint(name = "ubicacionPort")
    public Ubicacion getUbicacionPort() {
        return super.getPort(new QName("http://paqueteServicio/", "ubicacionPort"), Ubicacion.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns Ubicacion
     */
    @WebEndpoint(name = "ubicacionPort")
    public Ubicacion getUbicacionPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://paqueteServicio/", "ubicacionPort"), Ubicacion.class, features);
    }

    private static URL __getWsdlLocation() {
        if (UBICACION_EXCEPTION!= null) {
            throw UBICACION_EXCEPTION;
        }
        return UBICACION_WSDL_LOCATION;
    }

}
